import random

class Jugador:
  numero_jugadores = 0
  def __init__(self, name, cartas):
    self.name = name
    self.cartas = []
    Jugador.numero_jugadores += 1

  def celebrar(self):
    print("Yo " + self.name + " he ganado!!!!!!!")


class Cartas:
  def __init__(self, color, simbolo, cartas):
    self.color = color
    self.simbolo = simbolo
    self.cartas = []


class Mazo:
  def __init__(self, carta_1, carta_2, carta_3, carta_4):
    self.carta_1 = carta_1
    self.carta_2 = carta_2
    self.carta_3 = carta_3
    self.carta_4 = carta_4
    self.lista_cartas = [carta_1, carta_2, carta_3, carta_4]
    self.barajas_visibles= []
    self.mazo_completo = []


  def agrupar(self):    #ESTA FUNCION LA UTILICE PARA AGRUPAR LAS CARTAS

    for complete in self.lista_cartas:
      for carta in range(1, 14):
        self.mazo_completo.append([carta, complete.simbolo, complete.color])


  def barajar_cartas(self,lista,lista_dos):
    if len(lista_dos) == 5:
      return
    random.shuffle(lista)
    lista_dos.append(lista[0])
    return self.barajar_cartas(lista[1:],lista_dos)
  

  def barajar(self, jugador_1, jugador_2, jugador_3 = "", jugador_4 = ""):
    if Jugador.numero_jugadores == 1:
      self.barajar_cartas(self.mazo_completo, jugador_1.cartas)

    if Jugador.numero_jugadores == 2:
      self.barajar_cartas(self.mazo_completo, jugador_1.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_2.cartas)

    if Jugador.numero_jugadores == 3:
      self.barajar_cartas(self.mazo_completo, jugador_1.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_2.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_3.cartas)

    if Jugador.numero_jugadores == 4:
      self.barajar_cartas(self.mazo_completo, jugador_1.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_2.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_3.cartas)

      self.barajar_cartas(self.mazo_completo, jugador_4.cartas)
    


    

class Play:


  def __init__(self, mazo_cartas, player_1, player_2 = "", player_3 = "", player_4 = ""):
    self.mazo_cartas = mazo_cartas
    self.player_1 = player_1
    self.player_2 = player_2
    self.player_3 = player_3
    self.player_4 = player_4
 
    if Jugador.numero_jugadores == 1:
        self.lista_jugadores = [player_1.name, player_2, player_3, player_4]
    if Jugador.numero_jugadores == 2:
        self.lista_jugadores = [player_1.name, player_2.name, player_3, player_4]
    if Jugador.numero_jugadores == 3:
        self.lista_jugadores = [player_1.name, player_2.name, player_3.name, player_4]
    if Jugador.numero_jugadores == 4:
        self.lista_jugadores = [player_1.name, player_2.name, player_3.name, player_4.name]

    if Jugador.numero_jugadores == 1:
        self.barajas_jugadores = [player_1.cartas, player_2, player_3, player_4]
    if Jugador.numero_jugadores == 2:
        self.barajas_jugadores = [player_1.cartas, player_2.cartas, player_3, player_4]
    if Jugador.numero_jugadores == 3:
        self.barajas_jugadores = [player_1.cartas, player_2.cartas, player_3.cartas, player_4]
    if Jugador.numero_jugadores == 4:
        self.barajas_jugadores = [player_1.cartas, player_2.cartas, player_3.cartas, player_4.cartas]
  
    
  @staticmethod
  def quien_gano(lista):
    label_tres = False
    label_dos = False

    for tres in lista:
      coincidir=[]
      for detalle in lista:
        if detalle[0] == tres[0]:
          coincidir.append(detalle)
      
      if len(coincidir) == 3:
        if coincidir[0][0] == coincidir[1][0] == coincidir[2][0]:
          label_tres = True


      if len(coincidir) == 2:

        if coincidir[0][0] == coincidir[1][0]:
          label_dos = True

    if label_tres == True and label_dos == True:
      return True
    else:
      return False

  def Empezar(self):
    self.lista_jugadores = self.lista_jugadores[:Jugador.numero_jugadores]
    self.barajas_jugadores = self.barajas_jugadores[:Jugador.numero_jugadores]
    turnos= 0
    ultimo = len(mazo_cartas.barajas_visibles)-1
      
    while turnos < Jugador.numero_jugadores:

      if len(mazo_cartas.mazo_completo) == 0:
        if turnos >= Jugador.numero_jugadores:
          turnos = 0
              
        while turnos < Jugador.numero_jugadores:
          ultimo = len(mazo_cartas.barajas_visibles)-1
          print("\n Es su turno " + self.lista_jugadores[turnos] + " su maso de barajas es :")
          print(self.barajas_jugadores[turnos])
          mando=input("\n Presione la tecla 'Y' para coger del mazo visible: ")
          mando.lower()
          if mando == "y":
            print(mazo_cartas.barajas_visibles[ultimo],"<<<")
            mando=input("\n Desea escoger esta carta? Y / N: ")
            mando.lower()
            if mando == "y":
              print(self.barajas_jugadores[turnos])
              aviso = int(input("Cual carta desea eliminar? presione del 0 al 4: "))
              self.barajas_jugadores[turnos].append(mazo_cartas.barajas_visibles[ultimo])
              del(mazo_cartas.barajas_visibles[ultimo])
              mazo_cartas.barajas_visibles.append(self.barajas_jugadores[turnos][aviso])
              del(self.barajas_jugadores[turnos][aviso])
              print("\n mis cartas",self.barajas_jugadores[turnos])
            if mando == "N" or mando == "n":
              print("No escogio ninguna")
              print("\n cartas visibles",mazo_cartas.barajas_visibles[ultimo])
                   
          else:
            print("\n TECLA NO VALIDA EL SIGUIENTE JUGADOR!!!!!!!!! \n")
                    
                   
          if Play.quien_gano(self.barajas_jugadores[turnos]) == True:
            print("Ha ganado",self.lista_jugadores[turnos],"**********************************")
            break
                                         
          turnos += 1
          if turnos >= Jugador.numero_jugadores:
            turnos = 0
        break
                
                            

      print("\n Es su turno " + self.lista_jugadores[turnos] + " sus cartas son: ")
      print(self.barajas_jugadores[turnos])

      if len(mazo_cartas.barajas_visibles) == 0:
        mando=input("\n Escriba Y/y para escoger una carta del mazo ")
        if mando == "Y" or mando == "y":
          print(mazo_cartas.mazo_completo[0],"<<<")
        else:
          print("\n TECLA NO VALIDA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! \n")
          continue

        mando=input("\n Desea escoger esta carta? Y / N: ")
              
        if mando == "Y" or mando == "y":
          print(self.barajas_jugadores[turnos],"Mi barajas")
          aviso = int(input("\n Cual carta desea eliminar? presione del 0 al 4: "))
          print(self.barajas_jugadores[turnos])
          mazo_cartas.barajas_visibles.append(self.barajas_jugadores[turnos][aviso])
          del(self.barajas_jugadores[turnos][aviso])
          self.barajas_jugadores[turnos].append(mazo_cartas.mazo_completo[0])
          del(mazo_cartas.mazo_completo[0])
          print(self.barajas_jugadores[turnos],"Mi barajas")
          print("\n Barajas visibles",mazo_cartas.barajas_visibles[ultimo])
              
        if mando == "N" or mando == "n":
          mazo_cartas.barajas_visibles.append(mazo_cartas.mazo_completo[0])
          del(mazo_cartas.mazo_completo[0])
          print("\n cartas visibles",mazo_cartas.barajas_visibles[ultimo])
            
      else:
        ultimo = len(mazo_cartas.barajas_visibles)-1
        print("Maso ---> [X, X]","Cartas visibles ", mazo_cartas.barajas_visibles[ultimo])
        mando=input("\n Desea coger una carta del Mazo? O quieres coger una carta Visible? m/v: ")
        if mando == "m" or mando == "M":
          print(mazo_cartas.mazo_completo[0],"<<<")#baraja que me salio
          decidir=input("Desea escoger esta carta? y/n: ")
          if decidir == "y" or decidir == "Y":
            print(self.barajas_jugadores[turnos])#mis barajas
            aviso = int(input("Cual carta desea eliminar? presione del 0 al 4: "))
            mazo_cartas.barajas_visibles.append(self.barajas_jugadores[turnos][aviso])
            del(self.barajas_jugadores[turnos][aviso])#borra baraja
            self.barajas_jugadores[turnos].append(mazo_cartas.mazo_completo[0])
            del(mazo_cartas.mazo_completo[0])
            print("\n Mis barajas",self.barajas_jugadores[turnos])
          if decidir == "n" or decidir == "N":
            mazo_cartas.barajas_visibles.append(mazo_cartas.mazo_completo[0])
            del(mazo_cartas.mazo_completo[0])
        if mando == "v" or mando == "V":
          ultimo = len(mazo_cartas.barajas_visibles)-1
          print(self.barajas_jugadores[turnos])
          aviso = int(input("Cual carta desea eliminar? presione del 0 al 4: "))
          self.barajas_jugadores[turnos].append(mazo_cartas.barajas_visibles[ultimo])
          del(mazo_cartas.barajas_visibles[ultimo])
          mazo_cartas.barajas_visibles.append(self.barajas_jugadores[turnos][aviso])
          del(self.barajas_jugadores[turnos][aviso])
                
          print("\n mis cartas",self.barajas_jugadores[turnos])
        
      if Play.quien_gano(self.barajas_jugadores[turnos]) == True:
        print("Ha ganado",self.lista_jugadores[turnos],"**********************************")
        break
        
      turnos= turnos + 1
           
          
      if turnos == Jugador.numero_jugadores:
        turnos = 0


jugador_1 = Jugador("Wilson",[])
jugador_2 = Jugador("Maria", [])

corazones = Cartas("rojo","corazon", [])
diamante = Cartas("rojo","diamante", [])
espadas = Cartas("negro","espada", [])
treboles = Cartas("negro","trebol", [])

mazo_cartas = Mazo(corazones, diamante, espadas, treboles)
mazo_cartas.agrupar()
print(mazo_cartas.mazo_completo)

mazo_cartas.barajar(jugador_1, jugador_2)

partida = Play(mazo_cartas, jugador_1, jugador_2)

partida.Empezar()
